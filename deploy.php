<?php
namespace Deployer;

require 'recipe/common.php';

// Project name
set('application', 'Nodejs basic demo');

// Project repository
set('repository', 'git@gitlab.com:jv-it-workshop/deploy-a-basic-nodejs.git');

// Hosts
inventory('hosts.yml');
set('default_stage', 'staging');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', true); 

// Shared files/dirs between deploys 
set('shared_files', [
    'package-lock.json',
    'yarn.lock',
]);
set('shared_dirs', [
    'node_modules',
]);

// Writable dirs by web server 
set('writable_dirs', [

]);


// Tasks

desc('Deploy your project');
task('deploy', [
    'deploy:info',
    'deploy:prepare',
    'deploy:lock',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:writable',
    'packages:install',
    'deploy:clear_paths',
    'deploy:symlink',
    'deploy:unlock',
    'cleanup',
    'success'
]);

// [Optional] If deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

task('debug', function () {
    // run('node -v');
    // run('yarn --version');
    run('npm -v');
    run('cd {{release_path}} && npm install');
    // run('cd {{release_path}} && yarn');
});

task('packages:install', function () {
    // run('node -v');
    // run('yarn --version');
    // run('cd {{release_path}} && npm install');
    run('cd {{release_path}} && yarn');
});

// task('deploy:restart_pm2', function () {
//     run('cd {{deploy_path}}/current && pm2 reload ecosystem.config.js --only {{stage}}-basic-demo');
// });
// after('deploy:symlink', 'deploy:restart_pm2');