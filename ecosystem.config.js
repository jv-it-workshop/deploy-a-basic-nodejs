/* eslint-disable no-multi-str */
module.exports = {
  apps: [ 
    {
      name: 'staging-basic-demo',
      cwd: '/var/www/jv-wss/staging-basic-demo/current',
      script: 'index.js',
      max_memory_restart: '100M',
      exec_mode: 'cluster',
      instances: '1',
      autorestart: false,
      time: true,
      combine_logs: true,
      env: {
        NODE_ENV: 'staging',
      },
    },
    {
      name: 'production-basic-demo',
      cwd: '/var/www/jv-wss/production-basic-demo/current',
      script: 'index.js',
      max_memory_restart: '512M',
      exec_mode: 'cluster',
      instances: 'max',
      autorestart: true,
      time: true,
      combine_logs: true,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
